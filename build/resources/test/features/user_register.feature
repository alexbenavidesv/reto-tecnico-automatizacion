#Autor: Alex Benavides
  @stories
  Feature: User registration on the Utest platform
   @scenario1
    Scenario: Register on the Utest platform
    Given that Alex wants to register on the Utest platform
    When he enters all the required information
     | firstName | lastName | email            | birthMonth | birthDay | birthYear | city     | zip    | country  | mobileDevice | modelDevice | operatingSystemDevice | password     |
     | Alex      | Benavides| alexbenavides@correo.com | September  | 23       | 1999      | Bogota   | 230001 | Colombia | Xiaomi       | Redmi 9     | Android 10            | Password.123 |
    Then Registration ends when you see the Complete Setup button
     | completedText   |
     | Complete Setup  |