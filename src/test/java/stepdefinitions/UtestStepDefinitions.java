package stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.Data;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import question.Answer;
import tasks.*;

import java.util.List;

public class UtestStepDefinitions {

    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that Alex wants to register on the Utest platform$")
    public void thatAlexWantsToRegisterOnTheUtestPlatform() {
        OnStage.theActorCalled("Alex").wasAbleTo(OpenUp.thePage());
    }


    @When("^he enters all the required information$")
    public void heEntersAllTheRequiredInformation(List<Data> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(EnterPersonalInformation.thePage(data),
                EnterLocation.thePage(data),
                EnterDevices.thePage(data),
                EnterFinal.thePage(data));
    }

    @Then("^Registration ends when you see the Complete Setup button$")
    public void registrationEndsWhenYouSeeTheCompleteSetupButton(List<Data> data) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(data)));
    }
}
