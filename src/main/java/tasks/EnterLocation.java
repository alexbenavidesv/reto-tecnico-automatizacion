package tasks;

import model.Data;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;

import static userinterface.AddressPage.*;

import java.util.List;

public class EnterLocation implements Task {

    private List<Data> data;

    public EnterLocation(List<Data> data) {
        this.data = data;
    }

    public static EnterLocation thePage(List<Data> data) {
        return Tasks.instrumented(EnterLocation.class,data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(data.get(0).getCity()).into(CITY),
                Hit.the(Keys.ARROW_DOWN).into(CITY),
                Hit.the(Keys.ENTER).into(CITY),
                Enter.theValue(data.get(0).getZip()).into(ZIP),
                Click.on(COUNTRY_CONTAINER),
                Enter.theValue(data.get(0).getCountry()).into(COUNTRY).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(DEVICES_BUTTON));
    }
}
