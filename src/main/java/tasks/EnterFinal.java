package tasks;
import model.Data;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static userinterface.FinalPage.*;

public class EnterFinal implements Task {
    private List<Data> data;

    public EnterFinal(List<Data> data) {
        this.data = data;
    }

    public static EnterFinal thePage(List<Data> data) {
        return Tasks.instrumented(EnterFinal.class,data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(data.get(0).getPassword()).into(PASSWORD),
                Enter.theValue(data.get(0).getPassword()).into(CONFIRM_PASSWORD),
                Click.on(CHECKBOX1),
                Click.on(CHECKBOX2),
                Click.on(CHECKBOX3),
                Click.on(COMPLETE_BUTTON));
    }
}
