package tasks;
import model.Data;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import static userinterface.PersonalInformationPage.*;

import java.util.List;

public class EnterPersonalInformation implements Task {

    private List<Data> data;

    public EnterPersonalInformation(List<Data> data) {
        this.data = data;
    }

    public static EnterPersonalInformation thePage(List<Data> data) {
        return Tasks.instrumented(EnterPersonalInformation.class,data);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(data.get(0).getFirstName()).into(FIRST_NAME),
                Enter.theValue(data.get(0).getLastName()).into(LAST_NAME),
                Enter.theValue(data.get(0).getEmail()).into(EMAIL),
                SelectFromOptions.byVisibleText(data.get(0).getBirthMonth()).from(BIRTH_MONTH),
                SelectFromOptions.byVisibleText(data.get(0).getBirthDay()).from(BIRTHDAY),
                SelectFromOptions.byVisibleText(data.get(0).getBirthYear()).from(BIRTH_YEAR),
                Click.on(LOCATION_BUTTON));
    }
}
