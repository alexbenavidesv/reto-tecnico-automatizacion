package tasks;

import model.Data;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import static userinterface.DevicesPage.*;

import java.util.List;

public class EnterDevices implements Task {

    private List<Data> data;

    public EnterDevices(List<Data> data) {
        this.data = data;
    }

    public static EnterDevices thePage(List<Data> data) {
        return Tasks.instrumented(EnterDevices.class,data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(MOBILE_DEVICE_CONTAINER),
                Enter.theValue(data.get(0).getMobileDevice()).into(MOBILE_DEVICE).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(MODEL_CONTAINER),
                Enter.theValue(data.get(0).getModelDevice()).into(MODEL).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(OPERATING_SYSTEM_CONTAINER),
                Enter.theValue(data.get(0).getOperatingSystemDevice()).into(OPERATING_SYSTEM).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(LAST_STEP_BUTTON)
                );
    }
}
