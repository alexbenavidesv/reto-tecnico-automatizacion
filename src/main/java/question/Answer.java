package question;

import cucumber.api.java.en_scouse.An;
import model.Data;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import userinterface.FinalPage;

import java.util.List;

public class Answer implements Question<Boolean> {
    private List<Data> data;

    public static Answer toThe(List<Data> data) {
        return new Answer(data);
    }

    public Answer(List<Data> data) {
        this.data = data;
    }

    @Override
    public Boolean answeredBy(Actor actor) {


        boolean result;
        String finalText= Text.of(FinalPage.COMPLETE_BUTTON).viewedBy(actor).asString();

        if(finalText.equals(data.get(0).getCompletedText())){
            result=true;
        }
        else{
            result=false;
        }
        return result;

    }
}
