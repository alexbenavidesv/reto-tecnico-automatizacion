package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class AddressPage extends PageObject {
    public static final Target CITY=Target.the("Field to select the city")
            .located(By.id("city"));
    public static final Target ZIP=Target.the("Field to select the zip")
            .located(By.id("zip"));
    public static final Target COUNTRY_CONTAINER=Target.the("country container")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/div[1]/span/span[2]"));
    public static final Target COUNTRY=Target.the("Field to select the country")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/input[1]"));
    public static final Target DEVICES_BUTTON=Target.the("Button to go to the devices section")
            .located(By.xpath("//a[@class='btn btn-blue pull-right']"));
}
