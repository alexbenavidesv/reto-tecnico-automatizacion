package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PersonalInformationPage extends PageObject {
    public static final Target FIRST_NAME=Target.the("Field where the name is written")
            .located(By.id("firstName"));
    public static final Target LAST_NAME=Target.the("Field where the last name is written")
            .located(By.id("lastName"));
    public static final Target EMAIL=Target.the("Field where the email is written")
            .located(By.id("email"));
    public static final Target BIRTH_MONTH=Target.the("Field to select the month of birth")
            .located(By.id("birthMonth"));
    public static final Target BIRTHDAY=Target.the("Field to select the day of birth")
            .located(By.id("birthDay"));
    public static final Target BIRTH_YEAR=Target.the("Field to select the year of birth")
            .located(By.id("birthYear"));
    public static final Target LOCATION_BUTTON=Target.the("Button to go to the address section")
            .located(By.xpath("//a[@class='btn btn-blue']"));


}
