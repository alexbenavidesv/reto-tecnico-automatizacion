package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class FinalPage extends PageObject {
    public static final Target PASSWORD=Target.the("Field to create password")
            .located(By.id("password"));
    public static final Target CONFIRM_PASSWORD=Target.the("Field to confirm password")
            .located(By.id("confirmPassword"));
    public static final Target CHECKBOX1=Target.the("CHECKBOX")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[4]/label/span"));
    public static final Target CHECKBOX2=Target.the("CHECKBOX")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target CHECKBOX3=Target.the("CHECKBOX")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));
    public static final Target COMPLETE_BUTTON=Target.the("End button")
            .located(By.xpath("//*[@id=\"laddaBtn\"]/span"));

}
